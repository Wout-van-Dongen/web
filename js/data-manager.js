let loadJSON = function(url, callback){
    let httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function(){
        if(httpRequest.readyState === 4){
            if(httpRequest.status === 200 || httpRequest.status === 0){
                let data = JSON.parse(httpRequest.responseText);
                callback(data);
            };
        };
    };
    httpRequest.open('GET', url);
    httpRequest.send();
};


let buildLocationTable = function(data){
    // Defining the table
    let table = document.createElement('table');
    table.setAttribute('class', 'location-table');

    let headerRow = table.appendChild(
        document.createElement('tr')
    );
    headerRow.appendChild(
        document.createElement('th')
    ).appendChild(
        document.createTextNode('Name')
    );
    headerRow.appendChild(
        document.createElement('th')
    ).appendChild(
        document.createTextNode('Owner')
    );
    headerRow.appendChild(
        document.createElement('th')
    ).appendChild(
        document.createTextNode('X')
    );
    headerRow.appendChild(
        document.createElement('th')
    ).appendChild(
        document.createTextNode('Z')
    );
    // Building the table body
    for (let i in data.locations) {
        let cityRow = document.createElement('tr');
        let cityHeader = document.createElement('th');
        cityHeader.setAttribute('colspan', '4');
        cityHeader.setAttribute('class', 'city-header');
        table.appendChild(
            cityRow
        ).appendChild(
            cityHeader
        ).appendChild(
            document.createTextNode(data.locations[i].city)
        );
        for (var j in data.locations[i].items) {
            let itemRow = table.appendChild(
                document.createElement('tr')
            );
            itemRow.appendChild(
                document.createElement('td')
            ).appendChild(
                document.createTextNode(data.locations[i].items[j].label)
            );
            itemRow.appendChild(
                document.createElement('td')
            ).appendChild(
                document.createTextNode(data.locations[i].items[j].owner)
            );
            itemRow.appendChild(
                document.createElement('td')
            ).appendChild(
                document.createTextNode(data.locations[i].items[j].x)
            );
            itemRow.appendChild(
                document.createElement('td')
            ).appendChild(
                document.createTextNode(data.locations[i].items[j].z)
            );
            table.appendChild(itemRow);
        };
    };
    return table;
};
